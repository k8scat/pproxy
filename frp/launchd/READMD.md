# Launchd

Put plist file into `~/Library/LaunchAgents`

## Operations

### load

```bash
launchctl load ~/Library/LaunchAgents/frpc.plist
```

### unload

```bash
launchctl unload ~/Library/LaunchAgents/frpc.plist
```

### Status

```bash
launchctl list | grep frpc
```

Output:

| PID   | Status | Label |
| ----- | ------ | ----- |
| 21623 | 0      | frpc  |

> Status 说明：0 表示正常启动，正数表示启动存在错误，负数表示程序收到信号而终止了

## References

- [Launchd Tutorial](https://www.launchd.info/)
