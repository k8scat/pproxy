#!/bin/bash
set -e

VERSION="0.37.1"
OS="linux"
ARCH="amd64"

PKG_NAME="frp_${VERSION}_${OS}_${ARCH}"

function download_frp() {
  # curl -LO "https://github.com/fatedier/frp/releases/download/v${VERSION}/${PKG_NAME}.tar.gz"
  curl -LO http://cdn.qiniu.ncucoder.com/frp_0.37.1_linux_amd64.tar.gz
}

function install_frp() {
  tar zxf "${PKG_NAME}.tar.gz"
  sudo cp ${PKG_NAME}/frpc /usr/bin
  sudo cp ${PKG_NAME}/frps /usr/bin
  echo "frpc and frps installed"

  sudo mkdir -p /etc/frp
  echo "/etc/frp dir created"
}

function clean() {
    rm -rf ${PKG_NAME} ${PKG_NAME}.tar.gz
}

function setup_frp_service() {
  if [[ -d "systemd" ]]; then
    sudo cp systemd/* /usr/lib/systemd/system/
    echo "frp service already setup"
  fi
}

function main() {
  clean

  download_frp
  install_frp
  clean
  setup_frp_service

  clean
}

main "$@"
