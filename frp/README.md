# [frp](https://github.com/fatedier/frp)

## 使用 journalctl 查看 service 日志

```bash
# 查看 frps 的日志
sudo journalctl -u frps -f
```

## 重载 service 配置

```bash
sudo systemctl daemon-reload
```
