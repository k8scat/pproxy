SHELL := /bin/bash
CLASH_SUB := https://sub.srss.xyz/sub\?target\=clash\&new_name\=true\&url\=https://rss.srss.xyz/link/IVkrAjboId1wU9u8\?mu\=2\&insert\=false\&config\=https%3A%2F%2Fraw.githubusercontent.com%2FACL4SSR%2FACL4SSR%2Fmaster%2FClash%2Fconfig%2FACL4SSR_Online.ini

stop-v2ray-client:
	cd v2ray/client && \
	docker-compose down

start-v2ray-client:
	cd v2ray/client && \
	docker-compose down && \
	docker-compose up -d

update-clash-config:
	cd clash && \
	mv config.yaml config.yaml_$(shell date +"%Y%m%d%H%M%S") || true && \
	wget -O config.yaml $(CLASH_SUB)

start-clash:
	cd clash && \
	docker-compose down && \
	docker-compose up -d

stop-clash:
	cd clash && \
	docker-compose down

	
	