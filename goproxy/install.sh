#!/bin/bash
set -e

curl -LO https://github.com/snail007/goproxy/releases/download/v11.2/proxy-linux-amd64.tar.gz
mkdir proxy
tar proxy-linux-amd64.tar.gz -C proxy

rm -f proxy-linux-amd64.tar.gz
