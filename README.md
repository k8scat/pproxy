# 代理解决方案

## 付费代理

- **[PandaVPN](https://www.pantoto.xyz/r/13410971) 推荐**
- [ByWave](https://bywave.art/aff.php?aff=11244)
- [ggme.xyz](https://ggme.xyz/auth/register?code=7fJb)
- [paoluztz](https://owo-qvq-uvu-owo.xn--mes358a082apda.com/auth/register?code=ySoB)

## SSH 隧道

```bash
# SSH 动态端口转发
ssh -D 11111 -N -C ssh-proxy-server
```

## 常用代理配置

### Git 代理配置

使用 git 命令进行配置：

```bash
git config --global http.proxy 'http://host:port'
git config --global https.proxy 'http://host:port'

# 使用 basicAuth
git config --global http.proxyAuthMethod basic
git config --global http.proxy 'http://user:pass@host:port'
git config --global https.proxy 'http://user:pass@host:port'
```

直接修改 git 配置文件 `~/.gitconfig`：

```toml
[http]
    proxy = http://host:port
[https]
    proxy = http://host:port
```

### SSH 代理配置

> 主要用于 GitHub 加速

代理工具：[nc/ncat](https://nmap.org/)

编辑 SSH 配置文件 `~/.ssh/config`：

```ssh_config
Host github
    HostName github.com
    User git
    # Port 22
    IdentityFile ~/.ssh/id_rsa
    # CentOS
    ProxyCommand nc -v --proxy 127.0.0.1:11111 --proxy-type socks5 %h %p
    # MacOS
    # ProxyCommand nc -v -x 127.0.0.1:11111 %h %p
    
    # 额外配置
    StrictHostKeyChecking no
    LogLevel DEBUG
    Compression yes
```

查看 manpage：`man ssh_config`

### 终端代理脚本

> 实现快速开启/关闭代理，多个代理之间快速切换

```bash
function new_proxy() {
  local http_proxy=$1
  local socks5_proxy=$2
  echo "export https_proxy=$http_proxy \
  http_proxy=$http_proxy \
  ftp_proxy=$http_proxy \
  rsync_proxy=$http_proxy \
  all_proxy=$socks5_proxy \
  no_proxy=\"127.0.0.1,localhost,api.wakatime.com,goproxy.cn\""
}

alias unproxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy'

alias ssh_proxy="$(new_proxy socks5://127.0.0.1:11111 socks5://127.0.0.1:11111)"
alias panda_proxy="$(new_proxy http://127.0.0.1:41091 socks5://127.0.0.1:1090)"

# 使用 goproxy 开启一个本地代理
# sudo ./proxy http -t tcp -p "0.0.0.0:38080" --daemon --log http-proxy.log --forever
alias local_proxy="$(new_proxy http://127.0.0.1:38080 socks5://127.0.0.1:38081)"
```

### VSCode Proxy

```json
{
  "http.proxy": ""
}
```

## 常用工具

### telnet

> 检测IP、端口是否连通

```shell
# CentOS 7
# sudo yum install -y telnet
telnet 127.0.0.1 11111
```

## 代理工具

- [clash](https://github.com/Dreamacro/clash)
- [v2ray](https://v2fly.org)
- [goproxy](https://github.com/snail007/goproxy)

## 代理客户端

- [ClashX](https://github.com/yichengchen/clashX)

## 参考内容

- [v2ray/v2ray-core](https://github.com/v2ray/v2ray-core)
- [Dreamacro/clash](https://github.com/Dreamacro/clash)
- [yichengchen/clashX](https://github.com/yichengchen/clashX)
- [clash configuration](https://github.com/Dreamacro/clash/wiki/configuration)
- [v2fly](https://github.com/v2fly/)
- [v2fly guide](https://guide.v2fly.org/)
- [yanue/V2rayU](https://github.com/yanue/V2rayU)
