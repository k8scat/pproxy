#!/bin/bash
set -e

function new_proxy() {
  local http_proxy=$1
  local socks5_proxy=$2
  echo "export https_proxy=$http_proxy \
  http_proxy=$http_proxy \
  ftp_proxy=$http_proxy \
  rsync_proxy=$http_proxy \
  all_proxy=$socks5_proxy \
  no_proxy=\"127.0.0.1,localhost,api.wakatime.com,goproxy.cn\""
}

alias unproxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy'
# curl ip.sb
# curl ip.gs
# curl cip.cc

alias ssh_proxy="$(new_proxy socks5://127.0.0.1:11111 socks5://127.0.0.1)"
alias panda_proxy="$(new_proxy http://127.0.0.1:41091 socks5://127.0.0.1:1090)"
alias local_proxy="$(new_proxy http://127.0.0.1:38080 socks5://127.0.0.1:38081)"
