# SSH Proxy

```bash
sudo cp ssh-proxy.service /usr/lib/systemd/system/
sudo cp ones-saasdb.service /usr/lib/systemd/system/

sudo systemctl enable --now ssh-proxy
sudo systemctl enable --now ones-saasdb
```

## 设置 ulimit

[Socket accept - "Too many open files"](https://stackoverflow.com/questions/880557/socket-accept-too-many-open-files)

由于 `ulimit -n` 的值为 4096（同一时间最多可开启的文件数），导致代理失败（SSH 动态端口转发）

```bash
# limits 配置说明：/etc/security/limits.conf
vi /etc/security/limits.d/20-nproc.conf

hsowan soft nofile 65535
hsowan hard nofile 65535

# 重启机器
```
